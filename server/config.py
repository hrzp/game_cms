import logging


db = lambda: None
db.url = 'sqlite:///data.sqlite'
db.echo = False

flask = lambda: None
flask.secret_key = 'dev-secret-key'
flask.port = 5000
flask.debug = True
flask.static_url_path = '/static'
flask.static_folder = '../ui'

log = lambda: None
log.appname = 'app'
log.level = logging.DEBUG
log.filename = None
log.werkzeug_logger_level = logging.INFO


try:
    import local_config
except ImportError:
    pass


logging.basicConfig(
    filename=log.filename,
    format='%(asctime)s %(levelname)s %(name)s:%(filename)s:%(lineno)d: %(message)s',
    level=logging.WARNING)
root_logger = logging.getLogger(log.appname)
root_logger.setLevel(log.level)
logging.getLogger('werkzeug').setLevel(log.werkzeug_logger_level)
