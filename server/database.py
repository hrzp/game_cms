import config
import sqlalchemy as sa
import sqlalchemy.orm as saorm
import sqlalchemy.ext.declarative


Base = sqlalchemy.ext.declarative.declarative_base()
db_engine = sa.create_engine(config.db.url, echo=config.db.echo, encoding='utf-8', pool_recycle=3600)
Session = saorm.sessionmaker(bind=db_engine)


class User(Base):
    __tablename__ = 'user'
    id = sa.Column(sa.Integer, autoincrement=True, primary_key=True)
    username = sa.Column(sa.String(128), nullable=False)
    password = sa.Column(sa.String(128), nullable=False)

    def __repr__(self):
        return '%s(%d)' % (self.__class__.__name__, self.id or -1)


Base.metadata.create_all(db_engine)
